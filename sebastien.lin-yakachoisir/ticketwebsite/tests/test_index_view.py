from django.test import TestCase


class TestIndexView(TestCase):
    def test_exists(self):
        self.assertEqual(self.client.get('').status_code, 200)

    def test_links_to_event(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/evenements/"')

    def test_links_to_assos(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/assos/"')

    def test_links_to_contact(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/contact/"')

    def test_links_to_calendar(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/calendar/"')

    def test_links_to_login(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/login/"')

    def test_links_to_formulaire(self):
        response = self.client.get('')
        self.assertNotContains(response, 'href="/formulaire/"')

    def test_links_to_logout(self):
        response = self.client.get('')
        self.assertNotContains(response, 'href="/logout/"')

    def test_links_to_register(self):
        response = self.client.get('')
        self.assertContains(response, 'href="/signup/"')
