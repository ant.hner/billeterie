from django.test import TestCase

class TestAccessView(TestCase):

    def test_toEvent(self):
        response = self.client.get('/formulaire/')
        self.assertEqual(response.status_code, 200)

    def test_toAssos(self):
        response = self.client.get('/assos/')
        self.assertEqual(response.status_code, 200)

    def test_toContact(self):
        response = self.client.get('/contact/')
        self.assertEqual(response.status_code, 200)

    def test_toCalendar(self):
        response = self.client.get('/calendar/')
        self.assertEqual(response.status_code, 200)

    def test_toFormulaire(self):
        response = self.client.get('/formulaire/')
        self.assertEqual(response.status_code, 302)

    def test_toLogin(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_toEvent(self):
        response = self.client.get('/evenements/')
        self.assertEqual(response.status_code, 200)

    def test_toLogout(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 200)

    def test_toError(self):
        response = self.client.get('/azerty/')
        self.assertEqual(response.status_code, 404)

    def test_admin(self):
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 302)

    def test_signup(self):
        response = self.client.get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_search(self):
        response = self.client.get('/search/')
        self.assertEqual(response.status_code, 200)

    def test_profile(self):
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 302)


