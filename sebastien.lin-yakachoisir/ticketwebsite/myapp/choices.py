Choice_reccurence = (
    (1, ("Non")),
    (2, ("Oui")),
)

Choice_place = (
    (1, ("KB")),
    (2, ("Villejuif")),
    (3, ("Autre")),
)

Choice_boisson = (
    (1, ("Aucune boisson")),
    (2, ("Boisson sans alcool")),
    (3, ("Boisson avec alcool")),
)