from django import forms
from django.contrib.auth.models import User
from .models import Event, Association
from django.contrib.auth.forms import UserCreationForm
from .choices import *

class RegisterForm(UserCreationForm):
    email = forms.EmailField(label="Mail")
    username = forms.CharField(label="Nom d'utilisateur")
    password1 = forms.CharField(label="Mot de passe", widget=forms.TextInput(attrs={'type':'password'}))
    password2 = forms.CharField(label="Confirmation mot de passe", widget=forms.TextInput(attrs={'type':'password'}))
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class ContactForm(forms.Form):
    email = forms.EmailField(label="Votre mail: ")
    msg = forms.CharField(widget=forms.Textarea, label="Votre message: ")

class RegisterEventForm(forms.ModelForm):
	name = forms.CharField(label="Nom de l'événement")
	association = forms.ModelChoiceField(label="Nom de l'association", queryset=Association.objects.all())
	date = forms.DateField(label="Date de l'événement")
	start_time = forms.TimeField(label="Heure de début d'événement")
	end_time = forms.TimeField(label="Heure de fin d'événement")
	reccurence = forms.ChoiceField(choices=Choice_reccurence)
	oui_recc = forms.IntegerField(label="Si Oui : Fréquence (Jours)", required=False)
	oui_recc_2 = forms.DateField(label="Si Oui : Jusqu'au", required=False)
	tuteur = forms.CharField(label="Responsable sur place")
	tuteur_tel = forms.CharField(label="Téléphone du responsable sur place")
	tuteur_class = forms.CharField(label="Classe du responsable sur place")
	price_ionis = forms.FloatField(label="Prix de l'événement pour les étudiants du groupe IONIS") 
	price_extern = forms.FloatField(label="Prix de l'événement pour les externes")
	description = forms.CharField(label="Description de l'événement", widget=forms.Textarea)
	respo_civil = forms.ChoiceField(label="Avez-vous une assurance responsabilité civil ?", choices=Choice_reccurence)
	lieu_event = forms.ChoiceField(label="Lieu de l'événement", choices=Choice_place)
	lieu_autre = forms.CharField(label="Si Autre, précisez le lieu", required=False)
	salle_event = forms.CharField(label="Salle(s) prévue(s) pour l'événement")
	materiel_event = forms.CharField(label="Liste du materiel présent", required=False)
	boisson_event = forms.ChoiceField(label="Boisson(s) prévue(s)", choices=Choice_boisson)
	remarque_event = forms.CharField(label="Remarque spéciale concernant l'événement", required=False, widget=forms.Textarea)
	signature = forms.BooleanField(label="Accepter les conditions d'EPITA afin de finaliser l'inscription de l'événement")

	class Meta:
		model = Event
		fields = ['name', 'association','max_places', 'left_places', 'start_date', 'end_date','price_ionis', 'price_extern', 'image']

##class SubscriberForm(forms.ModelForm):
  ##  password = forms.CharField(widget=forms.PasswordInput)

    ##class Meta:
      ##  model = Subscriber
        ##fields = ('firstname', 'lastname', 'email', 'login', 'password')
