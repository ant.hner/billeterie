from __future__ import unicode_literals
from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.utils import timezone
from address.models import AddressField
from ticketwebsite import settings


class Subscriber(models.Model):
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, unique=True)
    login = models.CharField(max_length=50, unique=True)
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.login

class Association(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255,default="Description de l'association")
    image = models.ImageField(upload_to='img/', blank=True)

    def __str__(self):
        return self.name

    def get_absolute_path(self):
        return reverse("association_page", kwargs={"pk": self.pk})

    def image_url(self):
        if self.image != '/media/img/default.jpg' and hasattr(self.image, 'url'):
            return self.image.url
        else:
            return '/media/default.jpg'

class RankAssociation(models.Model):
    subscriber = models.ForeignKey(User, on_delete=models.CASCADE)
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    is_members = models.BooleanField(default=False)
    is_responsable = models.BooleanField(default=False)
    is_bureau = models.BooleanField(default=False)
    is_IonisInternal = models.BooleanField(default=False)
    is_IonisExternal = models.BooleanField(default=True)
    is_admin = models.BooleanField()
    is_me = models.BooleanField

    def __str__(self):
        return self.is_members

class Event(models.Model):
    name = models.CharField(max_length=255)
    association = models.ForeignKey(Association, on_delete=models.CASCADE)
    max_places = models.IntegerField()
    left_places = models.IntegerField()
    start_date = models.DateField(auto_now = False, auto_now_add=False)
    start_time = models.TimeField(u'Starting time', help_text=u'Starting time',
            default=timezone.now)
    end_date = models.DateField(auto_now = False, auto_now_add=False)
    end_time = models.TimeField(u'Final time', help_text=u'Final time',
            default=timezone.now)
    location = models.CharField(max_length=255, default="EPITA KB")
    status = models.CharField(max_length=200, default="En cours")
    description = models.CharField(max_length=500, default="Description de l'association")
    image = models.ImageField(upload_to='img/', null=True, blank=True, default='default.jpg')
    #likes = models.ManyToManyField(User, blank=True, related_name='event_likes')
    price_ionis = models.FloatField(default=False)
    price_extern = models.FloatField(default=False)
    validation = models.IntegerField(default=0)
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Evenement'
        verbose_name_plural = u'Evenement'

  #  def set_location(self, raw):
  #      self.location.raw = raw
   #     self.location.save()

    def get_absolute_path(self):
        return reverse("event_page", kwargs={"pk": self.pk})

    def get_absolute_url(self):
        return u'<a href=\"../evenements/%s/\">%s (%s)</a><br>%s:%s' % (self.pk, self.name, self.association, str(self.start_time.hour), str(self.start_time.minute))

    def image_url(self):
        if self.image != '/media/img/default.jpg' and hasattr(self.image, 'url'):
            return self.image.url
        else:
            return '/media/default.jpg'
    
    def hasplaces(self):
        if self.left_places == 0:
            return 'COMPLET'
        else:
            return 'Il reste ' + str(self.left_places) + ' places libres.'

class Ticket(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    subscriber = models.ForeignKey(User, on_delete=models.CASCADE)
    is_used = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    amount = models.IntegerField(default=1)

    def __str__(self):
        return self.event.name

    def get_total_cost(self):
        return self.price * self.amount
