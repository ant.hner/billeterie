from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.exceptions import MultipleObjectsReturned
from django.shortcuts import redirect
from django.contrib.auth import logout
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from decimal import Decimal
from paypal.standard.forms import PayPalPaymentsForm
from django.db.models import Q

from django.contrib import messages
from .forms import RegisterForm
from .forms import RegisterEventForm
from .forms import ContactForm
from django.views.generic.detail import DetailView
from .models import Event as Events

from datetime import datetime
from datetime import timedelta
import calendar
from django.views import generic
from django.utils.safestring import mark_safe

from .models import *
from .utils import EventCalendar
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.http import HttpResponse
import weasyprint

def index(request):
    return render(request,'index.html')

@login_required
def logged(request):
    auth = None
    try:
        auth = request.user.social_auth.get(provider="epita").extra_data
    except (MultipleObjectsReturned):
        pass

    context = {
        'user': request.user,
        'extra_data': auth,
    }
    return render(request, 'logged.html', context=context)

def logged2(request):
    return render_to_response('logged.html')

def mylogout(request):
    logout(request)
    return redirect('index')

def list_event(request):
    Status = ""
    if (request.user.groups.filter(name="Responsable des associations").exists()):
            Status = "assos"
    elif (request.user.groups.filter(name="Bureau d'association").exists()):
            Status = "bureau"
    else:
            Status = "None"
    context = {
            'events': Events.objects.all(),
            'status': Status
    }
    return render(request, 'list_evenement.html', context)

def list_assos(request):
    context = {
        'assos': Association.objects.all()
    }
    return render(request, 'list_assos.html', context)

def connection(request):
    return render(request, 'registration/login.html')

def event_page(request):
    return render_to_response('event_page.html')

def signup(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST);
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Compte crée avec succès ! Tu peux maintenant de connecter !')
            return redirect('connection')
    else:
        form = RegisterForm()
    return render(request, 'signup.html', {'form': form})

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST);
        if form.is_valid():
            mail = form.cleaned_data.get('email')
            msg = form.cleaned_data.get('msg')
            message = EmailMessage('Message from: '+mail,
                    "from: "+mail+  "\n\nMessage:\n\n"+msg ,
                           'billeterie.hyld@gmail.com',
                           ["billeterie.hyld@gmail.com"])
            message.send()
            messages.success(request, f'Votre message à bien été envoyé !')
            form = ContactForm()
    else:
        form = ContactForm()
    return render(request, 'contact.html', {'form': form})

@login_required
def formulaire(request):
    if request.method == 'POST':
        form = RegisterEventForm(request.POST)
        if form.is_valid():
            form.save()
            #name = form.cleaned_data.get('name')
            #association = form.cleaned_data.get('association')
            #start_date = form.cleaned_data.get('date')
            #end_date = form.cleaned_data.get('date')
            #ev = Event.objects.create(name=name, association=association, max_places=450, left_places=450, start_date=start_date, end_date=end_date)
            #ev.save()
            messages.success(request, f'L\'événement a été crée avec succès !')
            return redirect('index')
        else:
            print(form.errors)
            messages.warning(request,f'L\événement n\'a pas été crée. Veuillez remplir correctement le formulaire.')

    else:
        l = request.user.groups.values_list('name', flat=True);
        if (len(l) == 0):
            Status = "None"
        else:
            Status = l[0]
        if (Status == "None"or Status == "Inscrit"):
            return redirect("index")
        form = RegisterEventForm()
    return render(request, 'formulaire.html', {'form' : form})

@login_required
def profile(request):

    l = request.user.groups.values_list('name', flat=True);
    if (len(l) == 0):
        Status = "None"
    else:
        Status = l[0]

    context = {
            'tickets': Ticket.objects.filter(subscriber=request.user),
            'status': Status,
            'event' : Events.objects.filter(validation=0)
            }
    return render(request, 'profile.html', context)

class AssosDetailView(DetailView):
    models = Association
    template_name = 'association_page.html'
    queryset = Association.objects.all()

    def get_context_data(self, **kwargs):
        context = super(AssosDetailView, self).get_context_data(**kwargs)
        return context

class EventDetailView(DetailView):
    models = Events
    template_name = 'event_page.html'
    queryset = Events.objects.all()

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        context['evdetail'] = Events.objects.filter(pk=self.kwargs.get('pk'))
        context['user'] = self.request.user
        l = self.request.user.groups.values_list('name', flat=True);
        if (len(l) == 0):
            Status = "None"
        else:
            Status = l[0]
        context['status'] = Status
        if self.request.method == 'POST':
            form = registereventform(request.post)
            if form.is_valid():
                T = Ticket(event=Events.objects.filter(pk=self.kwargs.get('pk')), subscriber=self.request.user, is_used=False, price=context['evdetail'].price_ionis)
                T.save()
                redirect('index')
        return context

def get_list_tickets(request, pk):
        l = request.user.groups.values_list('name', flat=True);
        if (len(l) == 0):
            Status = "None"
        else:
            Status = l[0]
        if (Status == "None"or Status == "Inscrit"):
            return redirect("index")
        context = {
            "event" : Events.objects.get(pk=pk).name,
            "tickets" : Ticket.objects.all(),
        }
        html = render_to_string('pdf/ticket_list.html', context)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'ticket_list.pdf'
        pdf = weasyprint.HTML(string=html, base_url='http://8d8093d5.ngrok.io/users/process/').write_pdf(
            stylesheets=[weasyprint.CSS(string='body { font-family: serif}')])

        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "ticket_list.pdf"
            content = "inline; filename='%s'" %(filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" %(filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

def buy_ticket(request, pk):
    if request.method == 'POST':
        eve = Events.objects.get(pk=pk)
        T = Ticket(event=eve, subscriber=request.user, is_used=False,
        price=eve.price_ionis, amount=1)
        T.save()
        eve.left_places -= 1
        eve.save()

        context = {
            "event_name": T.event.name,
            "ticket_id": T.pk,
            "price": T.price,
            "amount": T.amount,
            "Customer_name": T.subscriber.username,
            "date": T.event.start_date,
        }
        html = render_to_string('pdf/ticket.html', context)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'billet.pdf'
        pdf = weasyprint.HTML(string=html, base_url='http://8d8093d5.ngrok.io/users/process/').write_pdf(
            stylesheets=[weasyprint.CSS(string='body { font-family: serif}')])

        message = EmailMessage('Achat de billet.',
            "Merci d'avoir acheté un billet dans notre billeterie.",
            'billeterie.hyld@gmail.com',
            [request.user.email])
        message.attach("billet.pdf", pdf, 'application/pdf')
        message.content_subtype = "html"
        message.send()
    return render(request, 'subscribed.html')

def disapprove_event(request, pk):
    eve = Events.objects.get(pk=pk);
    eve.delete()
    messages.success(request, f'L\'événement à été refusé avec succès !')
    return redirect('index')

def approve_event(request, pk):
    eve = Events.objects.get(pk=pk);
    eve.validation = 1
    messages.success(request, f'L\'événement à été validé avec succès !')
    eve.save()
    return redirect('index')
def named_month(month_number):
    """
    Return the name of the month, given the number.
    """
    return date(1900, month_number, 1).strftime("%B")

def this_month(request):
    """
    Show calendar of events this month.
    """
    today = datetime.now()
    return calendar(request, today.year, today.month)

class CalendarView(generic.ListView):
    model = Events
    template_name = 'cal/calendar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # use today's date for the calendar
        d = get_date(self.request.GET.get('day', None))

        # Instantiate our calendar class with today's year and date
        cal = EventCalendar()

        # Call the formatmonth method, which returns our calendar as a table
        html_cal = cal.formatmonth(d.year, d.month, withyear=True)
        context['calendar'] = mark_safe(html_cal)
        d = get_date(self.request.GET.get('month', None))
        context['prev_month'] = prev_month(d)
        context['next_month'] = next_month(d)
        return context

def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split('-'))
        return date(year, month, day=1)
    return datetime.today()

def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month

def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month

def process_payment(request, pk):
    eve = Events.objects.get(pk=pk)
    T = Ticket(event=eve, subscriber=request.user, is_used=False, price=eve.price_ionis, amount=1)
    T.save()
    host = request.get_host()
    context = {
        "event_name": T.event.name,
        "ticket_id": T.pk,
        "price": T.price,
        "amount": T.amount,
        "Customer_name": T.subscriber.username,
        "date": T.event.start_date,
    }
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': context["price"] * context["amount"],
        'item_name': 'Order {}'.format(context["ticket_id"]),
        'invoice': str(context["ticket_id"]),
        'currency_code': 'USD',
        'notify_url': 'http://{}{}'.format(host, reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format(host, reverse('payment_done', args=(context["ticket_id"],))),
        'cancel_return': 'http://{}{}'.format(host, reverse('payment_cancelled')),
    }

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'payment/process.html', {'form': form})

@csrf_exempt
def payment_done(request, pk):
    T = get_object_or_404(Ticket, id=pk)
    context = {
        "event_name": T.event.name,
        "ticket_id": T.pk,
        "price": T.price,
        "amount": T.amount,
        "Customer_name": T.subscriber.username,
        "date": T.event.start_date,
    }
    html = render_to_string('pdf/ticket.html', context)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'billet.pdf'
    pdf = weasyprint.HTML(string=html, base_url='http://8d8093d5.ngrok.io/users/process/').write_pdf(
        stylesheets=[weasyprint.CSS(string='body { font-family: serif}')])

    message = EmailMessage('Achat de billet.',
                           "Merci d'avoir acheté un billet dans notre billeterie.",
                           'billeterie.hyld@gmail.com',
                           [request.user.email])
    message.attach("billet.pdf", pdf, 'application/pdf')
    message.content_subtype = "html"
    message.send()
    return render(request, 'subscribed.html')

@csrf_exempt
def payment_canceled(request):
    return render(request, 'payment/canceled.html')

def searchquery(request):
    today = timezone.now().date()
    queryset_list = Events.objects.all()#.order_by("-timestamp")

    query = request.GET.get("q")
    if query:
        queryset_list = queryset_list.filter(
                            Q(name__icontains=query) |
                            Q(association__name=query)
        ).distinct()
    paginator = Paginator(queryset_list, 1)
    page_request_var = "page"
    page = request.GET.get(page_request_var)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)

    context = {
        "object_list": queryset,
        "title": "List",
        "page_request_var": page_request_var,
        "today": today,
    }
    return render(request, 'post_search.html', context)
