"""ticketwebsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views

from django.conf.urls import url, include
from django.urls import path
from social_django.urls import urlpatterns as social_django_urls
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static

from myapp import views as v

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', v.index, name='index'),
    url(r'^', include((social_django_urls, 'social_django'), namespace='social')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('evenements/', v.list_event, name='list_event'),
    url(r'^evenements/(?P<pk>\d+)/$', v.EventDetailView.as_view(), name='event_page'),
    url(r'^buy_ticket/(?P<pk>\d+)/$', v.buy_ticket, name='buy_ticket'),
    url(r'^assos/(?P<pk>\d+)/$', v.AssosDetailView.as_view(), name='association_page'),
    path('logged/', v.logged, name='logged'),
    path('logged2/', v.logged2, name='logged2'),
    path('assos/', v.list_assos, name='list_assos'),
    path('login/', auth_views.LoginView.as_view(redirect_authenticated_user=True,
        template_name="login.html"), name='connection'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('signup/', v.signup, name='signup'),
    path('profile/', v.profile, name='profile'),
    path('contact/', v.contact, name='contact'),
    path('formulaire/', v.formulaire, name='formulaire'),
    path('calendar/', v.CalendarView.as_view(), name='calendar'),
    url(r'^get_list_tickets/(?P<pk>\d+)/$', v.get_list_tickets, name='get_list_tickets'),
    url(r'^approve_event/(?P<pk>\d+)/$', v.approve_event, name='approve_event'),
    url(r'^disapprove_event/(?P<pk>\d+)/$', v.disapprove_event, name='disapprove_event'),
    path('paypal/', include('paypal.standard.ipn.urls'), name='paypal'),
    url(r'^process-payment/(?P<pk>\d+)/$', v.process_payment, name='process_payment'),
    url(r'^payment-done/(?P<pk>\d+)/$', v.payment_done, name='payment_done'),
    path('payment-cancelled/', v.payment_canceled, name='payment_cancelled'),
    url(r'^search/$', v.searchquery, name='search')
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
